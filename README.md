# iTunes Clone task

A site where you can search for songs from the SQLite Chinook database. In addition to this, several API calls can be made for the customers in this database.

## Getting Started

To get started with the app, go to:

https://noroff-accelerate-itunesclone.herokuapp.com/



## Functionalities

For the web-site, you can search for songs in the database, which will display all songs starting with what you typed. In addition to this, artist, album and genre is displayed for each song.

A collection of API calls to the customer API can be found in the JSON file `iTunes Clone.postman_collection`


## Authors

* **Simen Røstum** - *Initial work*