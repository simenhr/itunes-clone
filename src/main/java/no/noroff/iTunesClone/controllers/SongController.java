package no.noroff.iTunesClone.controllers;

import no.noroff.iTunesClone.data_access.SongRepository;
import no.noroff.iTunesClone.models.Genre;
import no.noroff.iTunesClone.models.Song;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class SongController {

    SongRepository songRepository = new SongRepository();

    @RequestMapping(value = "/songs", method = RequestMethod.GET)
    public ArrayList<Song> getAllSongs() {
        return songRepository.getAllSongs();
    }

    @RequestMapping(value = "/songs/{name}", method = RequestMethod.GET)
    public ArrayList<Song> getSongByName(@PathVariable String name) {
        return songRepository.getSongsByName(name);
    }
}
