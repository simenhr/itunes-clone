package no.noroff.iTunesClone.controllers;

import no.noroff.iTunesClone.data_access.ArtistRepository;
import no.noroff.iTunesClone.models.Artist;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class ArtistController {

    ArtistRepository artistRepository = new ArtistRepository();

    @RequestMapping(value = "/artists", method = RequestMethod.GET)
    public ArrayList<Artist> getAllArtists() {
        return artistRepository.getAllArtists();
    }

    @RequestMapping(value = "/artists/randoms", method = RequestMethod.GET)
    public ArrayList<Artist> getRandomArtists() {
        return artistRepository.getRandomArtists(5);
    }

    @RequestMapping(value = "/artists/{artistId}", method = RequestMethod.GET)
    public Artist getArtistById(@PathVariable String artistId) {
        return artistRepository.getArtistById(artistId);
    }

}
