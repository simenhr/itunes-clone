package no.noroff.iTunesClone.controllers;

import no.noroff.iTunesClone.data_access.GenreRepository;
import no.noroff.iTunesClone.models.Artist;
import no.noroff.iTunesClone.models.Genre;
import no.noroff.iTunesClone.models.Song;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class GenreController {

    GenreRepository genreRepository = new GenreRepository();

    @RequestMapping(value = "/genres", method = RequestMethod.GET)
    public ArrayList<Genre> getAllGenres() {
        return genreRepository.getAllGenres();
    }

    @RequestMapping(value = "/genres/randoms", method = RequestMethod.GET)
    public ArrayList<Genre> getRandomGenres() {
        return genreRepository.getRandomGenres(5);
    }

    @RequestMapping(value = "/genres/{name}", method = RequestMethod.GET)
    public Genre getgenreByName(@PathVariable String name) {
        return genreRepository.getGenreByName(name);
    }
}
