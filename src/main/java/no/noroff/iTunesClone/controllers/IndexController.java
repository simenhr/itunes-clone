package no.noroff.iTunesClone.controllers;

import no.noroff.iTunesClone.data_access.ArtistRepository;
import no.noroff.iTunesClone.data_access.GenreRepository;
import no.noroff.iTunesClone.data_access.SongRepository;
import no.noroff.iTunesClone.models.Artist;
import no.noroff.iTunesClone.models.Song;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class IndexController {

    ArtistRepository artistRepository = new ArtistRepository();
    GenreRepository genreRepository = new GenreRepository();
    SongRepository songRepository = new SongRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getIndexPage(Model model) {
        model.addAttribute("artists", artistRepository.getRandomArtists(5));
        model.addAttribute("genres", genreRepository.getRandomGenres(5));
        model.addAttribute("songs", songRepository.getRandomSongs(5));
        return "index";
    }

    @RequestMapping("/search")
    public String index(@RequestParam(value = "searchQuery", required = false) String search,
                        Model model) {// model.addAttribute("search", participant);
        model.addAttribute("searchResults", songRepository.getSearchedSongsByName(search));
        return "search";
    }
}
