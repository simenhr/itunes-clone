package no.noroff.iTunesClone.controllers;

import no.noroff.iTunesClone.data_access.CustomerRepository;
import no.noroff.iTunesClone.models.Customer;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    CustomerRepository customerRepository = new CustomerRepository();

    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value = "/api/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    @RequestMapping(value = "/api/customers", method = RequestMethod.PUT)
    public Boolean updateCustomer(@RequestBody Customer customer) {
        return customerRepository.updateCustomer(customer);
    }

    @RequestMapping(value = "/api/customers/countries", method = RequestMethod.GET)
    public String updateCustomer() {
        return customerRepository.numberOfCustomersInCountries();
    }

    @RequestMapping(value = "/api/customers/invoices", method = RequestMethod.GET)
    public String getTopSpenders() {
        return customerRepository.getTopSpenders();
    }

    @RequestMapping(value = "/api/customers/{id}/popular/genre", method = RequestMethod.GET)
    public String getTopGenres(@PathVariable String id) {
        return customerRepository.getTopGenre(id);
    }

}
