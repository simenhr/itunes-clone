package no.noroff.iTunesClone.models;

public class SearchedSong {

    private String songId;
    private String name;
    private String artist;
    private String album;
    private String genre;

    public SearchedSong() {
    }

    public SearchedSong(String songId, String name, String artist, String album, String genre) {
        this.songId = songId;
        this.name = name;
        this.artist = artist;
        this.album = album;
        this.genre = genre;
    }

    // Getters and setters

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
