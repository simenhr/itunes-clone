package no.noroff.iTunesClone.models;

public class Album {

    private String albumId;
    private String title;
    private String artistId;

    public Album() {
    }

    public Album(String albumId, String title, String artistId) {
        this.albumId = albumId;
        this.title = title;
        this.artistId = artistId;
    }

    // Getters and setters

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }
}
