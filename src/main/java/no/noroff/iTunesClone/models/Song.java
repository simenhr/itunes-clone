package no.noroff.iTunesClone.models;

public class Song {

    private String songId;
    private String name;
    private String albumId;
    private String genreId;

    public Song() {
    }

    public Song(String songId, String name, String albumId, String genreId) {
        this.songId = songId;
        this.name = name;
        this.albumId = albumId;
        this.genreId = genreId;
    }

    // Getters and setters

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }
}
