package no.noroff.iTunesClone.data_access;

import no.noroff.iTunesClone.models.Album;
import no.noroff.iTunesClone.models.Artist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ArtistRepository {

    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // CRUD

    public ArrayList<Artist> getAllArtists() {
        ArrayList<Artist> artists = new ArrayList<>();

        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT artistid, name FROM artist");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                artists.add( new Artist(
                        set.getString("artistid"),
                        set.getString("name")
                ));
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return artists;
    }

    public ArrayList<Artist> getRandomArtists(int numberOfArtists) {
        ArrayList<Artist> randomArtists = new ArrayList<>();
        ArrayList<Artist> allArtists = getAllArtists();
        Artist randomArtist;
        for (int i = 0; i < numberOfArtists; i++) {
            randomArtist = allArtists.get((int) Math.floor(Math.random()*allArtists.size()));
            randomArtists.add(randomArtist);
            allArtists.remove(randomArtist);
        }
        return randomArtists;
    }

    public Artist getArtistById(String artistId) {
        Artist artist = null;
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT artistId, name FROM artist WHERE artistId=?");
            prep.setString(1, artistId);
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                artist = new Artist(
                        set.getString("artistId"),
                        set.getString("name")
                );
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return artist;
    }

}
