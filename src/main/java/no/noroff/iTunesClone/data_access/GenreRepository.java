package no.noroff.iTunesClone.data_access;

import no.noroff.iTunesClone.models.Artist;
import no.noroff.iTunesClone.models.Genre;
import no.noroff.iTunesClone.models.Song;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GenreRepository {

    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // CRUD

    public ArrayList<Genre> getAllGenres() {
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT genreId, name FROM genre");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                genres.add( new Genre(
                        set.getString("genreId"),
                        set.getString("name")
                ));
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return genres;
    }

    public ArrayList<Genre> getRandomGenres(int numberOfGenres) {
        ArrayList<Genre> randomGenres = new ArrayList<>();
        ArrayList<Genre> allGenres = getAllGenres();
        Genre randomGenre;
        for (int i = 0; i < numberOfGenres; i++) {
            randomGenre = allGenres.get((int) Math.floor(Math.random()*allGenres.size()));
            randomGenres.add(randomGenre);
            allGenres.remove(randomGenre);
        }
        return randomGenres;
    }

    public Genre getGenreByName(String name) {
        Genre genre = null;
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT genreId, name FROM genre WHERE name=?");
            prep.setString(2, name);
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                genre = new Genre(
                        set.getString("genreId"),
                        set.getString("name")
                );
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return genre;
    }
}
