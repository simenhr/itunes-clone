package no.noroff.iTunesClone.data_access;

import no.noroff.iTunesClone.models.Artist;
import no.noroff.iTunesClone.models.Genre;
import no.noroff.iTunesClone.models.SearchedSong;
import no.noroff.iTunesClone.models.Song;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SongRepository {

    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // CRUD

    public ArrayList<Song> getAllSongs() {
        ArrayList<Song> songs = new ArrayList<>();
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT trackId, name, albumId, genreId FROM track");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                songs.add( new Song(
                        set.getString("trackId"),
                        set.getString("name"),
                        set.getString("albumId"),
                        set.getString("genreId")
                ));
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return songs;
    }

    public ArrayList<Song> getRandomSongs(int numberOfSongs) {
        ArrayList<Song> randomSongs = new ArrayList<>();
        ArrayList<Song> allSongs = getAllSongs();
        Song randomSong;
        for (int i = 0; i < numberOfSongs; i++) {
            randomSong = allSongs.get((int) Math.floor(Math.random()*allSongs.size()));
            randomSongs.add(randomSong);
            allSongs.remove(randomSong);
        }
        return randomSongs;
    }

    public Song getSongById(String trackId) {
        Song song = null;
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT trackId, name, albumId, genreId FROM track WHERE trackId=?");
            prep.setString(1, trackId);
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                song = new Song(
                        set.getString("artistId"),
                        set.getString("name"),
                        set.getString("albumId"),
                        set.getString("genreId")
                );
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return song;
    }

    public ArrayList<Song> getSongsByName(String name) {
        ArrayList<Song> songs = new ArrayList<>();
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT trackId, name, albumId, genreId FROM track WHERE name LIKE ?");
            prep.setString(1, name+"%");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                songs.add(new Song(
                        set.getString("trackId"),
                        set.getString("name"),
                        set.getString("albumId"),
                        set.getString("genreId")
                ));
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return songs;
    }

    public ArrayList<SearchedSong> getSearchedSongsByName(String name) {
        ArrayList<SearchedSong> songs = new ArrayList<>();
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT " +
                            "track.trackId, " +
                            "track.name AS trackName, " +
                            "album.title AS albumTitle, " +
                            "artist.name AS artistName, " +
                            "genre.name AS genreName " +
                            "FROM " +
                            "track " +
                            "INNER JOIN album ON album.albumid = track.albumid " +
                            "INNER JOIN artist ON artist.artistid = album.artistid " +
                            "INNER JOIN genre ON genre.genreid = track.genreid " +
                            "WHERE track.name LIKE ?");
            prep.setString(1, name+"%");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                songs.add(new SearchedSong(
                        set.getString("trackId"),
                        set.getString("trackName"),
                        set.getString("artistName"),
                        set.getString("albumTitle"),
                        set.getString("genreName")
                ));
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return songs;
    }
}
