package no.noroff.iTunesClone.data_access;

import no.noroff.iTunesClone.models.Album;
import no.noroff.iTunesClone.models.Song;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class AlbumRepository {

    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // CRUD

    public ArrayList<Album> getAllAlbums() {
        ArrayList<Album> albums = new ArrayList<>();
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT albumId, title, artistId FROM album");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                albums.add( new Album(
                        set.getString("albumId"),
                        set.getString("title"),
                        set.getString("artistId")
                ));
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return albums;
    }

    public Album getAlbumById(String albumId) {
        Album album = null;
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT albumId, title, artistId FROM album WHERE albumId=?");
            prep.setString(1, albumId);
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                album = new Album(
                        set.getString("albumId"),
                        set.getString("title"),
                        set.getString("artistId")
                );
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return album;
    }



}
