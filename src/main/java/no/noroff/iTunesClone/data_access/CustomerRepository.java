package no.noroff.iTunesClone.data_access;

import no.noroff.iTunesClone.models.Customer;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

public class CustomerRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // CRUD

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT customerId, firstName, lastName, country, postalCode, "+
                                    "phone, Email, supportRepId FROM customer");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                customers.add( new Customer(
                        set.getString("customerId"),
                        set.getString("firstName"),
                        set.getString("lastName"),
                        set.getString("country"),
                        set.getString("postalCode"),
                        set.getString("phone"),
                        set.getString("Email"),
                        set.getString("supportRepId")
                ));
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return customers;
    }

    public Boolean addCustomer(Customer customer) {
        Boolean success = false;
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO customer(customerId, firstName, lastName, "
                            + "country, postalCode, phone, Email, supportRepId) VALUES(?,?,?,?,?,?,?,?)");
            prep.setString(1, customer.getCustomerId());
            prep.setString(2, customer.getFirstName());
            prep.setString(3, customer.getLastName());
            prep.setString(4, customer.getCountry());
            prep.setString(5, customer.getPostalCode());
            prep.setString(6, customer.getPhone());
            prep.setString(7, customer.geteMail());
            prep.setString(8, customer.getSupportRepId());
            int result = prep.executeUpdate();
            success = (result != 0);
            System.out.println("Add customer went well!");

        } catch(Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return success;
    }

    public Boolean updateCustomer(Customer customer) {
        Boolean success = false;
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE customer SET customerId=?, firstName=?," +
                            " lastName=?, country=?, postalCode=?, phone=?, eMail=?, supportRepId=? WHERE customerId=?");
            prep.setString(1, customer.getCustomerId());
            prep.setString(2, customer.getFirstName());
            prep.setString(3, customer.getLastName());
            prep.setString(4, customer.getCountry());
            prep.setString(5, customer.getPostalCode());
            prep.setString(6, customer.getPhone());
            prep.setString(7, customer.geteMail());
            prep.setString(8, customer.getSupportRepId());
            prep.setString(9, customer.getCustomerId());
            int result = prep.executeUpdate();
            success = (result != 0);
            System.out.println("Update went well!");

        } catch(Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return success;
    }

    public String numberOfCustomersInCountries() {
        String result = "";
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT COUNT(*) AS count, country " +
                            "FROM customer " +
                            "GROUP BY country " +
                            "ORDER BY count DESC");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                result += set.getString("country") + ": " + set.getString("count") + "\n";
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return result;
    }

    public String getTopSpenders() {
        String result = "";
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT customer.FirstName, customer.LastName, invoice.Total " +
                            "FROM customer INNER JOIN invoice " +
                            "ON customer.CustomerId=invoice.CustomerId " +
                            "GROUP BY customer.LastName " +
                            "ORDER BY invoice.Total DESC");
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                result += set.getString("FirstName") +
                         " " + set.getString("LastName") + ": " + set.getString("Total") + "\n";
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return result;
    }

    public String getTopGenre(String id) {
        String result = "";
        try {
            //connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT COUNT(*) AS count, customer.customerId, customer.FirstName, customer.LastName, genre.name " +
                            "FROM customer " +
                            "INNER JOIN invoice ON customer.customerId=invoice.customerId " +
                            "INNER JOIN invoiceLine ON invoice.invoiceId=invoiceLine.invoiceId " +
                            "INNER JOIN track ON invoiceLine.trackId=track.trackId " +
                            "INNER JOIN genre ON track.genreId=genre.genreId WHERE customer.customerId=?" +
                            "GROUP BY genre.name " +
                            "ORDER BY count DESC LIMIT 1");
            prep.setString(1, id);
            ResultSet set = prep.executeQuery();
            while(set.next()) {
                result += set.getString("FirstName") + "'s most popular genre is: " + set.getString("name");
            }
            System.out.println("Get All Went Well!");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return result;
    }
}
