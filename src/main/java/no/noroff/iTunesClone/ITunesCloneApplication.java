package no.noroff.iTunesClone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ITunesCloneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ITunesCloneApplication.class, args);
	}

}
